const btnMobile = document.querySelector('.menu-mobile')
const menu = document.querySelector('.menu')
const nav = document.querySelector('.menuCont')

btnMobile.addEventListener('click',()=>{
    let btnMobileImg = btnMobile.querySelector('img')
    

    btnMobileImg.setAttribute('src', !menu.classList.contains('ativo') ? '/img/icon-close.svg' : '/img/icon-hamburger.svg');


    

    menu.classList.toggle('ativo')
    nav.classList.toggle('ativo')
})